# VueJS - Shopping Cart - No Components

## Description du projet

Ce projet est une application simple de panier d'achat réalisée avec VueJS et Tailwind CSS. Il est destiné à être un exercice pratique pour les étudiants en développement web.

## Comment commencer

Importez ce dépôt et suivez les étapes décrites dans les issues de GitLab. Chaque issue correspond à une étape spécifique du développement de l'application. Tenez à jour votre board !

## Issues et étapes du projet

### SPRINT 1 - Mise en place de l'app

#### Issue 1: Chargement de VueJS et création de l'instance de Vue

- Charger la bibliothèque VueJS
- Créer une nouvelle instance de Vue

#### Issue 2: Définition des Data pour les produits

- Définir un tableau de produits dans les `data` de l'instance Vue

#### Issue 3: Affichage des produits dans le template

- Utiliser la directive `v-for` pour afficher les produits dans le template

#### Issue 4: Initialisation artificielle du localstorage

- Créer manuellement un `localStorage` avec un article dans le panier pour visualiser son contenu

#### Issue 5: Affichage des produits du panier

- Afficher les produits ajoutés au panier

#### Issue 6: Affichage du prix total

- Calculer et afficher le prix total des articles dans le panier

### SPRINT 2 - Mise en place de l'interactivité

#### Issue 7: Ajout au panier

- Implémenter la fonctionnalité pour ajouter des produits au panier

#### Issue 8: Retrait du panier

- Implémenter la fonctionnalité pour retirer des produits du panier

#### Issue 9: Modifier les quantités de produits

- Ajouter la possibilité de modifier la quantité d'un produit dans le panier

#### Issue 10: Mettre à jour le localStorage

- Mettre à jour le `localStorage` lorsque le panier est modifié

## Auteurs

- Pascal Lacroix

## Licence

Ce projet est sous licence MIT.
